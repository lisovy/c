#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[])
{
	char buff1[] = "ASDFASDF";
	char *buff2  = "ASDFASDF";

	printf("char buff1[] = \"ASDFASDF\";\n");
	printf("char *buff2  = \"ASDFASDF\";\n\n");

	printf("sizeof(buff1) = %i   [Characters + null termination]\n", sizeof(buff1));
	printf("sizeof(buff2) = %i   [Just the pointer]\n", sizeof(buff2));
	printf("strlen(buff1) = %i   [Just characters without null termination]\n", strlen(buff1));
	printf("strlen(buff2) = %i\n", strlen(buff2));
	return 0;
}

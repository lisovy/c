#include <stdio.h>
#include <stdint.h>
#include <arpa/inet.h> /* nhtohl(), htonl() */

void memprint(void* from, size_t len)
{
	int i;
	int j;

	printf("[From: %p Length: %d]\n", from, len);

	/* Align the address even when we will be reading in bytes */
	from = (void *)((intptr_t)from & (intptr_t)~0x3);

	for (i = 0; i < len; i += 4) {
		printf("%p: ", ((char *)from + i));

		for (j = 0; j < 4; j++)
			printf("%02hhX ", *((char *)from + i + j));

		printf("\n");
	}
}

struct t1 {
	int a;
	char b;
	int c;
	char d;
};

struct t2 {
	int a;
	char b;
	int c;
	char d;
} __attribute__ ((packed));

struct t3 {
	int a;
	char b __attribute__ ((aligned (32)));
	int c;
	char d __attribute__ ((aligned (32)));
};

struct t4 {
	unsigned int a;
	unsigned int b;
};

int main(int argc, char* argv[])
{
	intptr_t sp;

	struct t1 t1 = {
		.a = 0x55555555,
		.b = 0xAA,
		.c = 0x55555555,
		.d = 0xAA,
	};

	struct t2 t2 = {
		.a = 0x55555555,
		.b = 0xAA,
		.c = 0x55555555,
		.d = 0xAA,
	};

	struct t3 t3 = {
		.a = 0x55555555,
		.b = 0xAA,
		.c = 0x55555555,
		.d = 0xAA,
	};

	struct t4 t4;

	printf("Normal struct: \n");
	memprint(&t1, sizeof(t1));

	printf("\nPacked struct: \n");
	memprint(&t2, sizeof(t2));

	printf("\nStruct with aligned fields: \n");
	memprint(&t3, sizeof(t3));

	/* Network order = Big endian
	   x86           = Little endian
	   On another architectures, this example will not be much impressive */
	t4.a = htonl(0xaabbccdd);
	t4.b = 0xaabbccdd;

	printf("\nWriting into fields with different endianness: \n");
	memprint(&t4, sizeof(t4));

//	asm("movl %%esp, %0" : "=r" (sp)); /* Get the stack pointer address */
//	memprint((void *)sp, 255);

	return 0;
}

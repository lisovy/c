#include <stdio.h>



int main(int argc, char* argv[])
{
	char *str = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
	int i = 5;

	printf("*str = \"ABCDEFGHJKLMNOPQRSTUVWXYZ\"\n");
	printf("str[i]            = %c\n", str[i]);
	printf("i[str]            = %c\n", i[str]);

	printf("str[5]            = %c\n", str[5]);
	printf("5[str]            = %c\n", 5[str]);

	printf("*(str + i)        = %c\n", *(str + i));
	printf("*((int *)str + i) = %c\n", *((int *)str + i));

	return 0;
}
